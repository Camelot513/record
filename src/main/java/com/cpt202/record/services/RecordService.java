package com.cpt202.record.services;

import com.cpt202.record.model.Records;
import com.cpt202.record.repositories.RecordRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RecordService {
    @Autowired
    private RecordRepo recordRepo;

    public Records newRecord(Records records){
        return recordRepo.save(records);
    }

    public List<Records> getRecordList(){
        return recordRepo.findAll();
    }

    public List<Records> getUnderwayList() {
        return recordRepo.findByState("Underway");
    }

    public Records getRecordDetails(int recordId){
        return recordRepo.findById(recordId).orElse(null);
    }

    public Page<Records> getRecords(int page, int size){
        Pageable pageable = PageRequest.of(page - 1, size);
        return recordRepo.findAll(pageable);
    }

    public void deleteRecord(int id) {
        recordRepo.deleteById(id);
    }
//    public List<Records> getUnderwayRecordList(){
//        return recordRepo.findByState(Records.State.UNDERWAY);
//    }
}
