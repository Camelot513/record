package com.cpt202.record.model;

import javax.persistence.*;
import java.sql.Timestamp;

@Entity
public class Records {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;
    private Timestamp time;
    @Column(name = "service_name")
    private String Service_name;
    @Column(name = "service_number")
    private int Service_number;
    @Column(name = "total_money")
    private double Total_money;
    @Column(name = "state")
    private String state;
    private String Pet_type;
    private String Pet_size;
    private int Grommer_ID;
    private String Account;
    //private boolean finish;
    public Records(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Timestamp getTime() {
        return time;
    }

    public void setTime(Timestamp time) {
        this.time = time;
    }

    public String getService_name() {
        return Service_name;
    }

    public void setService_name(String service_name) {
        this.Service_name = service_name;
    }

    public int getService_number() {
        return Service_number;
    }

    public void setService_number(int service_number) {
        this.Service_number = service_number;
    }

    public double getTotal_money() {
        return Total_money;
    }

    public void setTotal_money(double total_money) {
        this.Total_money = total_money;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getPet_type() {
        return Pet_type;
    }

    public void setPet_type(String pet_type) {
        Pet_type = pet_type;
    }

    public String getPet_size() {
        return Pet_size;
    }

    public void setPet_size(String pet_size) {
        Pet_size = pet_size;
    }

    public int getGrommer_ID() {
        return Grommer_ID;
    }

    public void setGrommer_ID(int grommer_ID) {
        Grommer_ID = grommer_ID;
    }

    public String getAccount() {
        return Account;
    }

    public void setAccount(String account) {
        Account = account;
    }
}
