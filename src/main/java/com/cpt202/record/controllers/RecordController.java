package com.cpt202.record.controllers;


import com.cpt202.record.model.Records;
import com.cpt202.record.services.RecordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
//@RequestMapping("/record")

public class RecordController {

    @Autowired
    private RecordService recordService;


//    @GetMapping("/list")
//    //也可以在这里加一个@RequestMapping("/team"),这样下面的GetMapping等就不用加team
//    public String getList(@RequestParam(name = "state", required = false) String state, Model model){
//        List<Records> records;
//        if (state != null && state.equals("Underway")){
//            records = recordService.getUnderwayList();
//        }else {
//            records = recordService.getRecordList();
//        }
//        //model.addAttribute("recordlist", recordService.getRecordList());
//        model.addAttribute("recordlist", records);
//        return "allRecords";
//    }

    @RequestMapping("/record/list")
    public String getRecordsPage(@RequestParam(defaultValue = "1") int page, @RequestParam(defaultValue = "5") int size, Model model){
        Page<Records> records = recordService.getRecords(page, size);
        model.addAttribute("recordlist", records.getContent());
        model.addAttribute("currentPage", page);
        model.addAttribute("totalPages", records.getTotalPages());
        model.addAttribute("size", size);
        return "allRecords";
    }

    @GetMapping("/record/list/details/{id}")
    public String getRecordDetails(@PathVariable("id") int recordId, Model model){
        Records records = recordService.getRecordDetails(recordId);
        model.addAttribute("recordlist", records);
        return "recordDetails";
    }

    @GetMapping("/record/list/delete")
    public String deleteRecord(@RequestParam(value = "id") int id){
        recordService.deleteRecord(id);
        return "redirect:/record/list";
    }


//    @GetMapping("/list")
//    public String getList(Model model){
//        model.addAttribute("recordlist", recordService.getRecordList());
//        return "allRecords";
//    }
//    @GetMapping("/record/underway")
//    public String getUnderwayList(Model model){
//        // 获取所有未付款的订单记录
//        model.addAttribute("underwaylist", recordService.getUnderwayRecordList());
//        return "underwayRecords";
//    }


}
