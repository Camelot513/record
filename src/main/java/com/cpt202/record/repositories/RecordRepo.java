package com.cpt202.record.repositories;

import com.cpt202.record.model.Records;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
@Repository
public interface RecordRepo extends JpaRepository<Records, Integer> {
    //List<Records> findByState(Records.State State);
    Page<Records> findAll(Pageable pageable);
    List<Records> findByState(String state);
}
